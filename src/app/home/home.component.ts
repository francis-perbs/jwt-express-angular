import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";

import { User } from "../shared/user/user";
import { UserService } from "../shared/user/user.service";

@Component({
  selector: 'app-home',
  providers: [UserService],
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  public user: User;

  constructor(private router: Router, private userService: UserService) {
        this.user = new User();
        this.user.email = 'admin@test.com';
        this.user.password = 'password';
    }

  ngOnInit() {
  }


  login() {
      this.userService.login(this.user).subscribe(
            () => {
                //this.isLoading = false;
                this.router.navigate(['users'])
            },
            (error) =>  {
                //this.isLoading = false;
                alert('Unfortunately we could not find your account.');
            }
        );
  }

}
